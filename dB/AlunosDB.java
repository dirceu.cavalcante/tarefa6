package dB;

import models.Aluno;
//import java.util.List;
import java.util.ArrayList;

public class AlunosDB {
    private ArrayList<Aluno> alunosList= new ArrayList<Aluno>();

    public ArrayList<Aluno> getAlunosList(){
        return alunosList;
    }

    public void addNovoAluno (Aluno novoAluno){
        alunosList.add(novoAluno);
    }
}
