package models;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;


public class Aluno {
    public String nomeAluno;
    private List<String> chamadaAula = new ArrayList<>(); 
    public List<Date> diaAula = new ArrayList<>();
   
    public String getNomeAluno(){
        return this.nomeAluno;
    }

    public void setNomeAluno(String nomeAluno){
        this.nomeAluno = nomeAluno;
    }

    public List<Date> getDiaAula(){
        return this.diaAula;
    }

    public List<String> getChamadaAula(){
        return this.chamadaAula;
    }
    
    public void addDiaAula (Date diaAula){
        this.diaAula.add(diaAula);
    }

    public void addChamadaAula (String chamadaAula){
        this.chamadaAula.add(chamadaAula);
    }

}
