import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.text.SimpleDateFormat;

import dB.AlunosDB;
import models.Aluno;


public class Main {
    
    static AlunosDB alunosDB = new AlunosDB();

    public static void main(String[] args) throws Exception{
        
        int opcao;
        
        
        do{
            System.out.println("\n-----  DIÁRIO DE CLASSE  -----");
            System.out.println("1 - Cadastrar Aluno");
            System.out.println("2 - Inserir Chamada do Dia");
            System.out.println("3 - Listar Diário de Classe");
            System.out.println("0 - Sair");
            System.out.print("Opção: ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();
            process(opcao);   

        }while (opcao != 0);
    }

    public static void process (int opcao) throws Exception{
        Aluno aluno = new Aluno();
        Scanner scanner = new Scanner(System.in);
        ArrayList<Aluno> listaAlunos = alunosDB.getAlunosList();
        switch (opcao){
        
            case 1:{
                System.out.print("\n- Digite o nome do aluno: ");
                String nomeAluno = scanner.nextLine();
                aluno.setNomeAluno(nomeAluno);
                listaAlunos.add(aluno);
                break;
                }

                case 2: {
                    
                    System.out.print("\n- Digite a data da aula (dia/mês): ");
                    String data = scanner.next();
                    Date diaAula =new SimpleDateFormat("dd/MM").parse(data);
                    
                    for (Aluno aluno1 : listaAlunos){
                        aluno1.addDiaAula(diaAula);
                        System.out.println("");
                        System.out.print("- Digite a presença de "+aluno1.getNomeAluno()+": ");
                        String presenca = scanner.next();
                        aluno1.addChamadaAula(presenca);
                    }
                    break;
                }
                case 3: {
                        System.out.println("===== DIÁRIO DE CLASSE =====");
                    for (Aluno alunos : listaAlunos){
                        List<String> chamada = alunos.getChamadaAula();
                        List<Date> ListaDiaAula = alunos.getDiaAula();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
                        System.out.println(alunos.getNomeAluno());
                        
                        for (Date diaAula : ListaDiaAula){
                            System.out.print(sdf.format(diaAula)+" - ");
                        }
                        System.out.println("");
                        System.out.print("  ");
                        for (String presenca : chamada){
                            System.out.print(presenca + "       ");
                        }
                        System.err.print("\n--------------------\n");
                    }
                    break;
                }
        }
    }

}
